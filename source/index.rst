.. handbook1 documentation master file, created by
   sphinx-quickstart on Sat Dec  7 17:51:21 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to handbook1's documentation!
=====================================

.. toctree::
   :maxdepth: 2

   hello
   head

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
